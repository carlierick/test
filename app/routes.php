<?php
declare(strict_types=1);

use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use Slim\App;
use Slim\Interfaces\RouteCollectorProxyInterface as Group;

return function (App $app) {
    $app->options('/{routes:.*}', function (Request $request, Response $response) {
        // CORS Pre-Flight OPTIONS Request Handler
        return $response;
    });

    $app->get('/', function (Request $request, Response $response) {
        $response->getBody()->write('Ebanx - Test');
        return $response;
    });       

    $app->post('/reset', function (Request $request, Response $response) {
        
        /* Cria a Conta 300 inicial */
        $_account["300"]['balance'] = 0;
        $_account["300"]['limit']   = 100; /* limite disponivel */

        
        /* Tmp File reset accounts balance */
        $file_handle = fopen('../accounts.json' , 'w');
        fwrite($file_handle, json_encode($_account));
        fclose($file_handle);
        
        $response->getBody()->write('OK');
        return $response;
    });

    $app->get('/balance', function (Request $request, Response $response) {

        /* Tmp File accounts balance */
        $file = file_get_contents('../accounts.json');
        $_accounts = json_decode( $file , true );                    
        $paramns    = $request->getQueryParams();
        $conta      = $paramns['account_id'];
           
        /* Search balance in accounts */
        if(isset($_accounts[$conta])){

            $saldo['balance'] =  $_accounts[$conta]['balance'];
            $saldo['limit'] =  $_accounts[$conta]['limit'];

            $payload = json_encode($saldo);
            $response->getBody()->write($payload);
            return $response
                ->withHeader('Content-Type', 'application/json')->withStatus(200);

        }else{           
           $newResponse = $response->withStatus(404);
           $response->getBody()->write("0");
        }        
        return $newResponse;
    });

   
    $app->addBodyParsingMiddleware();
    $app->addRoutingMiddleware();
    $app->addErrorMiddleware(true, true, true);
    
    $app->post('/event', function (Request $request, Response $response) {    

        $data = $request->getParsedBody();
        if(isset($data['type'])){
            
            /* Tmp File accounts balance */
            $file = file_get_contents('../accounts.json');
            $_accounts = json_decode( $file , true );                              
                        
            switch($data['type']){
                case "deposit":                        
                    $_erros = [];
                    if(!isset($data['destination'])){
                        $_erros[] = 'Param [destination] not exist';
                    }
                    if(!isset($data['amount']) OR !is_int($data['amount'])){
                        $_erros[] = 'Param [amount] not exist or not number';
                    }
                    if(count($_erros) <= 0){
                        /* check is account exist */
                        if(isset($_accounts[$data['destination']])){
                            /* add balance */
                            $_accounts[$data['destination']]['balance'] +=  $data['amount'];
                        }else{
                            /* create account  */
                            $_accounts[$data['destination']]['balance'] =  $data['amount'];
                        }
                        
                        /* Save info */                       
                        $file_handle = fopen('../accounts.json' , 'w');
                        fwrite($file_handle, json_encode($_accounts));
                        fclose($file_handle);
                        
                        /* result */
                        $transaction = [];
                        $transaction['destination']['id']       = $data['destination'];
                        $transaction['destination']['balance']  = $_accounts[$data['destination']]['balance'];
                        
                        /* return result account */
                        $payload = json_encode($transaction);
                        $response->getBody()->write($payload);
                        return $response
                                  ->withHeader('Content-Type', 'application/json')->withStatus(201);                                                     
                    }else{
                        $payload = json_encode($_erros);
                        $response->getBody()->write($payload);
                        return $response
                                  ->withHeader('Content-Type', 'application/json')->withStatus(406);
                    }

                    break;
                    
                    
                case "withdraw":
                        
                    $_erros = [];
                    if(!isset($data['origin'])){
                        $_erros[] = 'Param [origin] not exist';
                    }
                    if(!isset($data['amount']) OR !is_int($data['amount'])){
                        $_erros[] = 'Param [amount] not exist or not number';
                    }
                    if(count($_erros) <= 0){
                        /* Search Account */
                        if(isset($_accounts[$data['origin']])){


                            if($_accounts[$data['origin']]['balance']   > $data['amount'] ){
                                /* withdraw */
                                $_accounts[$data['origin']]['balance']       -=  $data['amount'];

                                /* Save info */
                                $file_handle = fopen('../accounts.json' , 'w');
                                fwrite($file_handle, json_encode($_accounts));
                                fclose($file_handle);

                                /* result */
                                $transaction = [];
                                $transaction['origin']['id']       = $data['origin'];
                                $transaction['origin']['balance']  = $_accounts[$data['origin']]['balance'];
                                $transaction['origin']['limit']    = $_accounts[$data['origin']]['limit'];

                                /* return result account */
                                $payload = json_encode($transaction);
                                $response->getBody()->write($payload);
                                return $response
                                    ->withHeader('Content-Type', 'application/json')->withStatus(201);

                            }else if(($_accounts[$data['origin']]['limit']  + $_accounts[$data['origin']]['balance'] )  >= $data['amount']){

                                /* withdraw */
                                $valor_usar_limite = $data['amount'] - $_accounts[$data['origin']]['balance'];
                                $_accounts[$data['origin']]['balance'] = 0;
                               // $_accounts[$data['origin']]['limit_usado'] = $valor_usar_limite;
                                $_accounts[$data['origin']]['limit']   -=  $valor_usar_limite;

                                /* Save info */
                                $file_handle = fopen('../accounts.json' , 'w');
                                fwrite($file_handle, json_encode($_accounts));
                                fclose($file_handle);

                                /* result */
                                $transaction = [];
                                $transaction['origin']['id']       = $data['origin'];
                                $transaction['origin']['balance']  = $_accounts[$data['origin']]['balance'];
                                $transaction['origin']['limit']  = $_accounts[$data['origin']]['limit'];

                                /* return result account */
                                $payload = json_encode($transaction);
                                $response->getBody()->write($payload);
                                return $response
                                    ->withHeader('Content-Type', 'application/json')->withStatus(201);

                            }else{
                                $newResponse = $response->withStatus(406);
                                $response->getBody()->write("insufficient funds");
                                return $newResponse;
                            }
                                                        

                        }else{
                            /*
                            # Withdraw from non-existing account
                            POST /event {"type":"withdraw", "origin":"200", "amount":10}
                             * 404 0
                             */                             
                            $newResponse = $response->withStatus(404);
                            $response->getBody()->write("0");
                            return $newResponse;
                        }
                                                
                        
                    }else{
                        $payload = json_encode($_erros);
                        $response->getBody()->write($payload);
                        return $response
                                  ->withHeader('Content-Type', 'application/json')->withStatus(406);
                    }
                    
                    break;
                case "transfer":
                    
                    $_erros = [];
                    if(!isset($data['origin'])){
                        $_erros[] = 'Param [origin] not exist';
                    }
                    if(!isset($data['amount']) OR !is_int($data['amount'])){
                        $_erros[] = 'Param [amount] not exist or not number';
                    }
                    if(!isset($data['destination'])){
                        $_erros[] = 'Param [destination] not exist';
                    }
                    
                    if(count($_erros) <= 0){                        
                        if(isset($_accounts[$data['origin']]) && isset($_accounts[$data['destination']])){
                            
                            /* Check is balance */


                            if($_accounts[$data['origin']]['balance'] >= $data['amount'] ){
                                
                                $_accounts[$data['origin']]['balance']       -=  $data['amount'];
                                $_accounts[$data['destination']]['balance']  +=  $data['amount'];
                                                                
                                /* Save info */                       
                                $file_handle = fopen('../accounts.json' , 'w');
                                fwrite($file_handle, json_encode($_accounts));
                                fclose($file_handle);  
                                
                                /* result */
                                $transaction = [];
                                $transaction['origin']['id']            = $data['origin'];
                                $transaction['origin']['balance']       = $_accounts[$data['origin']]['balance'];
                                $transaction['destination']['id']       = $data['destination'];
                                $transaction['destination']['balance']  = $_accounts[$data['destination']]['balance'];

                                /* return result account */
                                $payload = json_encode($transaction);
                                $response->getBody()->write($payload);
                                return $response
                                          ->withHeader('Content-Type', 'application/json')->withStatus(201);                                  
                                
                            }else{
                                
                                /* Verifica se tenho limite suficioente para usar  */
                                $value_use_limit = $_accounts[$data['origin']]['balance'] - $data['amount'];
                                if( $value_use_limit <= $_accounts[$data['origin']]['limite']){
                                    $_accounts[$data['origin']]['limite'] = $_accounts[$data['origin']]['limite'] - $value_use_limit;
                                    /* result */
                                    $transaction = [];
                                    $transaction['origin']['id']            = $data['origin'];
                                    $transaction['origin']['balance']       = $_accounts[$data['origin']]['balance'];
                                    $transaction['origin']['limit']         = $_accounts[$data['origin']]['limite'];
                                    $transaction['destination']['id']       = $data['destination'];
                                    $transaction['destination']['balance']  = $_accounts[$data['destination']]['balance'];

                                    /* return result account */
                                    $payload = json_encode($transaction);
                                    $response->getBody()->write($payload);
                                    return $response
                                        ->withHeader('Content-Type', 'application/json')->withStatus(201);

                                }else{
                                    $newResponse = $response->withStatus(406);
                                    $response->getBody()->write("insufficient funds");
                                    return $newResponse;
                                }


                            }
                            
                        }else{
                            $newResponse = $response->withStatus(404);
                            $response->getBody()->write("0");
                            return $newResponse;                            
                        }
                    
                    }else{
                        $payload = json_encode($_erros);
                        $response->getBody()->write($payload);
                        return $response
                                  ->withHeader('Content-Type', 'application/json')->withStatus(406);
                    }

                    break;
                default:
                    $response->getBody()->write('Param [type] not exist');
                    break;
            }
        }else{
            $response->getBody()->write('Param [type] not found');
        }
        return $response;
           
    });
   
};
